using Multiplayer;
using Steamworks;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.HumanFallFlat
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class HumanFallFlatPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly NetHost netHost;
        private readonly NetPlayer netPlayer;

        internal Human human;

        public HumanFallFlatPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
        }

        public HumanFallFlatPlayer(Human human) : this(human.player.host.connection.ToString(), human.player.host.name)
        {
            // Store player objects
            this.human = human;
            netHost = human.player.host;
            netPlayer = human.player;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets/sets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            human = gamePlayer as Human;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address
        {
            get
            {
                SteamNetworking.GetP2PSessionState((CSteamID)netHost.connection, out P2PSessionState_t sessionState);
                uint ip = sessionState.m_nRemoteIP;
                return string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255);
            }
        }

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => netHost?.connection != null && NetGame.kickedUsers.Contains(netHost.connection);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => IsServer || netHost?.connection != null && NetGame.instance.allclients.Contains(netHost);

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => human != null && human.state == HumanState.Dead;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => human != null && human.state == HumanState.Unconscious;

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => human.player.isLocalPlayer;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason, TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned && IsConnected)
            {
                // Ban and kick player
                NetGame.instance.Kick(netHost); // TODO: Save bans somewhere for persistence
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                NetStream netStream = NetGame.BeginMessage(NetMsgId.Kick);
                try
                {
                    NetGame.instance.SendReliable(netHost, netStream);
                }
                finally
                {
                    if (netStream != null)
                    {
                        netStream.Release();
                    }
                }
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                NetGame.kickedUsers.Remove(netHost.connection);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => throw new NotImplementedException(); // TODO: Implement when possible
            set => throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => throw new NotImplementedException(); // TODO: Implement when possible
            set => throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount) => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            if (human != null)
            {
                human.unconsciousTime = amount;
                human.state = HumanState.Unconscious;
            }
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill()
        {
            if (human != null)
            {
                human.state = HumanState.Dead;
                Respawn();
            }
        }

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (human?.player?.host != null)
            {
                // Update name in-game
                netHost.name = newName;
                netPlayer.nametag.textMesh.text = newName;

                // Update name in uMod
                human.IPlayer.Name = newName;
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            if (human != null)
            {
                global::Game.instance.ResetPlayer(human);
            }
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn() => Respawn(new Position());

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            if (human != null)
            {
                global::Game.instance.Respawn(human, pos.ToVector3());
            }
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => human.transform.position.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && human?.state != null)
            {
                human.state = HumanState.Spawning;
                human.SetPosition(new Vector3(x, y, z));
                human.Reset();
                human.state = HumanState.Idle;
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                if (!HumanFallFlatExtension.Dedicated && IsServer)
                {
                    NetChat.OnReceive(NetGame.instance.local.hostId, prefix, message);
                }
                NetStream netStream = NetGame.BeginMessage(NetMsgId.Chat);
                try
                {
                    netStream.WriteNetId(NetGame.instance.local.hostId);
                    netStream.Write(prefix ?? string.Empty);
                    netStream.Write(message);
                    NetGame.instance.SendReliable(netHost, netStream);
                }
                finally
                {
                    if (netStream != null)
                    {
                        netStream.Release();
                    }
                }
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Chat and Commands
    }
}
