﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.Init")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.IOnPlayerChat(Multiplayer.NetHost,System.String,System.String)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.IOnPlayerConnected(Human)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.IOnPlayerDisconnected(Multiplayer.NetHost)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.IOnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.OnPluginLoaded(uMod.Plugins.Plugin)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.OnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.OnServerSave")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlat.OnServerShutdown")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Ban(System.String,System.TimeSpan)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Kick(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Heal(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Hurt(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Rename(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Respawn(uMod.Common.Position)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Teleport(System.Single,System.Single,System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.HumanFallFlat.HumanFallFlatConsolePlayer.Teleport(uMod.Common.Position)")]
